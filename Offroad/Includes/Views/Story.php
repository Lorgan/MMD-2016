<?php
    class Story
    {
       public $stories = array(
            array(
                "StoryID"=>"1",
                "StoryDate"=>"13. marts 2011", 
                "StoryName"=>"Opera at the beach",
                "StoryDescription"=>"Det var dejligt på stranden mmmhh....",
                "StoryImage"=>"img/about/1.jpg"
                ),
            array(
                "StoryID"=>"2",
                "StoryDate"=>"14. marts 2011", 
                "StoryName"=>"Metal at the beach", 
                "StoryDescription"=>"Det var dejligt på stranden mmmhh....",
                "StoryImage"=>"img/about/2.jpg"
                ),
            array(
                "StoryID"=>"3",
                "StoryDate"=>"15. marts 2011", 
                "StoryName"=>"Heavy Metal at the beach", 
                "StoryDescription"=>"Det var dejligt på stranden mmmhh....",
                "StoryImage"=>"img/about/3.jpg"

                ),
            array(
                "StoryID"=>"4",
                "StoryDate"=>"16. marts 2011", 
                "StoryName"=>"Country at the beach", 
                "StoryDescription"=>"Det var dejligt på stranden mmmhh....",
                "StoryImage"=>"img/about/4.jpg"
                ),
            array(
                "StoryID"=>"5",
                "StoryDate"=>"17. marts 2011", 
                "StoryName"=>"Tekno at the beach", 
                "StoryDescription"=>"Det var dejligt på stranden mmmhh....",
                "StoryImage"=>"img/about/5.jpg"
                ),

        );

        function returnAllStories()
        {
            //funktion der returnerer alle historier fra arrayet.
                $storyCount = count($this->stories);
            foreach($this->stories as $st){
                $storyCount--;
                if ($storyCount % 2 == 0) {
                    echo "<li>";
                } else {
                    echo "<li class='timeline-inverted'>";
                }

                $keys = array_keys($st);
                $values = array_values($st);

                echo "
                            <div class='timeline-image'>
                                <img class='img-circle img-responsive' src='" . $values[4] . "' alt=''>
                            </div>
                                <div class='timeline-panel'>
                                  <div class='timeline-heading'>
                                      <h4>" . $values[1] . "</h4>
                                      <h4 class='subheading'>" . $values[2] . "</h4>
                                </div>
                                <div class='timeline-body'>
                                    <p class='text-muted'>" . $values[3] . "</p>
                                </div>
                            </div>
                     </li>";

                
            }
        }
    }


?>