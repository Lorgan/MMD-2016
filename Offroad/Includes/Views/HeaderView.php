<header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Alle kan bidrage med historier fra Offroad 2017!</div>
                <div class="intro-heading">HISTORIER FRA OFFROAD 2017</div>
                <a href="#historier" class="page-scroll btn btn-xl">Læs mere her</a>
            </div>
        </div>
</header>